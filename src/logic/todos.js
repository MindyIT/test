export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const REMOVE_ITEM = 'qgo/assessment/REMOVE_ITEM';
export const SELECT_ITEM = 'qgo/assessment/SELECT_ITEM';
export const FILTER_ITEMS = 'qgo/assessment/FILTER_ITEMS';

export const addItem = content => {
  return { type: ADD_ITEM, content };
};

export const removeItem = item => ({ type: REMOVE_ITEM, item });
export const selectItem = item => ({ type: SELECT_ITEM, item });
export const filterItems = filterBy => ({ type: FILTER_ITEMS, filterBy });

export const initialState = {
  items: [
    { id: 1, content: 'Call mum' },
    { id: 2, content: 'Buy cat food' },
    { id: 3, content: 'Water the plants' },
  ],
};

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;
      const newItem = {
        id: nextId,
        content: action.content,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };

    case REMOVE_ITEM:

      return {
        ...state,
        items: state.items.filter(item => item.id !== action.item.id),
      };

    case SELECT_ITEM:
      const newItems = state.items.map(item => {
        if (item.id === action.item.id) {
          item.completed = !action.item.completed;
        }
        return item;
      });

      return {
        ...state,
        items: newItems,
      };

    case FILTER_ITEMS:
      return {...state, filterBy: action.filterBy};

    default:
      return state;
  }
};

export default reducer;
