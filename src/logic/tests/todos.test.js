import reducer, { initialState, addItem, removeItem, selectItem, filterItems } from '../todos';

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first' },
        { id: 2, content: 'second' },
      ]
    }
    const mockAction = addItem('third');
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toEqual(3);
    expect(result.items[2].content).toEqual('third');
  });

  it('should remove item on REMOVE_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first' },
        { id: 2, content: 'second' },
      ]
    }
    const mockAction = removeItem({id: 2});
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(1);
    expect(result.items[0].id).toEqual(1);
  });


  it('should change completed state on SELECT_ITEM', () => {
    const state = {
      items: [
        { id: 1, content: 'first', completed: true },
        { id: 2, content: 'second' },
      ]
    }
    let mockAction = selectItem(state.items[1]);
    let result = reducer(state, mockAction);
    expect(result.items).toHaveLength(2);
    expect(result.items[1].completed).toBe(true);

    mockAction = selectItem(result.items[1]);
    result = reducer(state, mockAction);
    expect(result.items[1].completed).toBe(false);
  });

  it('should set filterBy on FILTER_ITEMS', () => {
    const state = {
      items: []
    }
    let mockAction = filterItems('completed');
    let result = reducer(state, mockAction);
    expect(result.filterBy).toBe('completed');

    mockAction = filterItems();
    result = reducer(state, mockAction);
    expect(result.filterBy).toBeUndefined();
  });

});
