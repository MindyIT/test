import React from 'react';
import { shallow, mount } from 'enzyme';
import { Item } from '../index';

const defaultProps = {
  item: {id: 3, content: 'test'},
  selectItem: f => f,
  removeItem: f => f
};

describe('Item', () => {
  it('renders without crashing', () => {
    shallow(<Item {...defaultProps} />);
  });

  it('should display content', () => {
    const renderedItem = shallow(<Item {...defaultProps} />);
    expect(renderedItem.find('span').text()).toBe('test');
  });

  it('should be unchecked', () => {
    const renderedItem = shallow(<Item {...defaultProps} />);
    expect(renderedItem.find('.completed')).toHaveLength(0);
  });

  it('should be checked as completed', () => {
    const item = {id: 3, content: 'test', completed: true};
    const renderedItem = shallow(<Item {...defaultProps} item={item} />);
    expect(renderedItem.find('.completed')).toHaveLength(1);
  });

  it('should have remove button', () => {
    const renderedItem = shallow(<Item {...defaultProps} />);
    expect(renderedItem.find('.remove')).toHaveLength(1);
  });

  it('should trigger remove function', () => {
    const removeItem = jest.fn();
    const renderedItem = mount(<Item {...defaultProps} removeItem={removeItem} />);
    renderedItem.find('.remove').simulate('click');
    expect(removeItem.mock.calls.length).toBe(1);
  });

  it('should trigger completed function', () => {
    const selectItem = jest.fn();
    const renderedItem = mount(<Item {...defaultProps} selectItem={selectItem} />);
    renderedItem.find('input').simulate('change');
    expect(selectItem.mock.calls.length).toBe(1);
  });

});
