import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './styles.css';

import { removeItem, selectItem } from '../../logic/todos';

export const Item = ({ item, removeItem, selectItem }) => {
  item.completed = !!item.completed;
  const completedClass = item.completed ? 'completed' : '';

  return (
    <div className={completedClass}>
      <input type="checkbox" checked={item.completed} onChange={selectItem} />
      <span>{item.content}</span>
      <div className="remove" onClick={removeItem}> X </div>
    </div>
  );
};

Item.propTypes = {
  item: PropTypes.object.isRequired,
  removeItem: PropTypes.func.isRequired,
  selectItem: PropTypes.func.isRequired
};

const mapStateToProps = (state, props) => {
  return { items: state.todos.items, item: props.item };
};

const mapDispatchToProps = (dispatch, props) => {
  const item = props.item;
  return {
    removeItem: () => dispatch(removeItem(item)),
    selectItem: () => dispatch(selectItem(item))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Item);
