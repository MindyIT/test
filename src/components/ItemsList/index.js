import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './styles.css';

import { filterItems } from '../../logic/todos';
import Item from '../Item';

export const ItemsList = ({ items, filterBy, filterItems }) => {
  let selectedFilter = {all: 'selected'};

  if (filterBy === 'completed') {
    items = items.filter(item => item.completed);
    selectedFilter = {completed: 'selected'};
  }

  if (filterBy === 'notCompleted') {
    items = items.filter(item => !item.completed);
    selectedFilter = {notCompleted: 'selected'};
  }

  return (
    <div>
      <div className="filter">
        Show:
        <button
          className={selectedFilter.all}
          onClick={() => filterItems()}>All</button>
        <button
          className={selectedFilter.completed}
          onClick={() => filterItems('completed')}>Completed</button>
        <button
          className={selectedFilter.notCompleted}
          onClick={() => filterItems('notCompleted')}>Not completed</button>
      </div>

      <ul className="itemsList-ul">
        {items.length < 1 && <p id="items-missing">Add some tasks above.</p>}
        {items.map(item => <li key={item.id}><Item item={item} /></li>)}
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  filterItems: PropTypes.func.isRequired,
  filterBy: PropTypes.string
};

const mapDispatchToProps = dispatch => ({
  filterItems: filterBy => dispatch(filterItems(filterBy))
});

const mapStateToProps = (state, props) => {
  return { items: state.todos.items, filterBy: state.todos.filterBy };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
